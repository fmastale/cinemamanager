import java.util.ArrayList;
import java.util.Scanner;

public class Cinema {
    private final int rows;
    private final int seats;
    private final Scanner scanner = new Scanner(System.in);

    private ArrayList<ArrayList<String>> cinema;
    private int totalIncome;
    private int purchasedTickets;
    private double bookedPlaces;
    private int currentIncome;

    public Cinema(int rows, int seats) {
        this.rows = rows;
        this.seats = seats;
    }

    public void create() {
        ArrayList<ArrayList<String>> cinemaRoom = new ArrayList<>();

        for (int i = 0; i < rows; i++) {
            cinemaRoom.add(new ArrayList<>());
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < seats; j++) {
                cinemaRoom.get(i).add("S ");
            }
        }

        cinema = cinemaRoom;
    }

    public void run() {
        boolean shouldRun = true;

        while (shouldRun) {
            System.out.println();
            System.out.println("1. Show the seats");
            System.out.println("2. Buy a ticket");
            System.out.println("3. Show statistics");
            System.out.println("0. Exit");

            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    showSeats();
                    break;
                case 2:
                    buyTicket();
                    break;
                case 3:
                    showStatistics();
                    break;
                case 0:
                    shouldRun = false;
                    break;
                default:
                    System.out.println("No such option!");
            }
        }
    }

    private void showSeats() {
        System.out.println("Cinema:");
        System.out.print("  ");
        for (int i = 1; i <= seats; i++) {
            System.out.print(i + " ");
        }
        System.out.println();

        for (int i = 0; i < cinema.size(); i++) {
            System.out.print(1 + i + " ");
            for (int j = 0; j < cinema.get(i).size(); j++) {
                System.out.print(cinema.get(i).get(j));
            }
            System.out.println();
        }
    }

    private void buyTicket() {
        int row;
        int seat;
        boolean placeIsNotBooked = true;

        do {
            System.out.println("Enter a row number:");
            row = scanner.nextInt();

            System.out.println("Enter a seat number in that row:");
            seat = scanner.nextInt();

            if (row <= rows && seat <= seats){
                if (cinema.get(row - 1).get(seat - 1).equals("B ")) {
                    System.out.println("That ticket has already been purchased!");
                } else {
                    cinema.get(row - 1).set(seat - 1, "B ");
                    placeIsNotBooked = false;
                }
            } else {
                System.out.println("Wrong input");
            }
        } while (placeIsNotBooked);

        int ticketPrice = countTicketPrice(row);
        System.out.printf("Ticket price: $%d", ticketPrice);

        updateStatistics(ticketPrice);
    }

    private void showStatistics() {
        double allSeats = rows * seats;
        double percentageOfBookedPlaces = (bookedPlaces / allSeats) * 100;
        totalIncome = countTotalIncome();

        System.out.println();
        System.out.printf("Number of purchased tickets: %d%n", purchasedTickets);
        System.out.printf("Percentage: %.2f%s%n", percentageOfBookedPlaces, "%");
        System.out.printf("Current income: $%d%n", currentIncome);
        System.out.printf("Total income: $%d%n", totalIncome);
    }

    private int countTicketPrice(int row) {
        int seatsInRoom = rows * seats;
        int ticketPrice;

        if (seatsInRoom <= 60) {
            ticketPrice = 10;
        } else {
            int frontRows = rows/2;

            ticketPrice = row <= frontRows ? 10 : 8;
        }

        return ticketPrice;
    }

    private int countTotalIncome() {
        int totalIncome;
        int allPlacesInCinema = rows * seats;

        if (allPlacesInCinema <= 60) {
            totalIncome = allPlacesInCinema * 10;
        } else {
            int frontRows = rows/2;
            int backRows = rows - frontRows;

            totalIncome = (frontRows * seats * 10) + (backRows * seats * 8);
        }

        return totalIncome;
    }

    private void updateStatistics(int ticketPrice) {
        purchasedTickets += 1;
        bookedPlaces += 1;
        currentIncome += ticketPrice;
    }
}
